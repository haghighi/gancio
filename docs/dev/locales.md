---
layout: default
title: Internationalization
permalink: /dev/locales
parent: Hacking
nav_order: 7
---

## Internationalization 

We're self-hosting an instance of [weblate](https://weblate.gancio.org) you can use to help us with translations.